#ifndef separateMolecule_lib
#define separateMolecule_lib

//#include "../struct.h"
#include "monomer.h"
#include "soma_util.h"

int separate_molecule(const char* const filename,bead_data*b,polymer_data*poly);


void cross_link_monomer1(int32_t** poly_arch_tmp_pointer,polymer_data*const poly, unsigned int poly_arch_len,unsigned int monomerN, unsigned int monomerM);

int write_polyarch(int start,int tmp_molecule_index,int * checked_monomer,int* mapping_list,Monomer***tmp_beads_pointer,int32_t***tmp_poly_arch_pointer,int sequence,bead_data*b,polymer_data*poly,int** store_all_crosslink,int n_poly,int*tmp_poly_offset);
int find_molecule(int molecule_index,int*** store_all_crosslink_pointer,int bond_max,int sequence,polymer_data*poly,int ** checked_monomer_pointer,int mono_i);
int find_the_lastnode(int* number_of_nodes_pointer, int* bond_number_total,int* bond_nodes_checked);
int find_unchecked_bond(int bond_num_tot,int* bond_total,int checked_bonds,int**checked_bonds_node,int number_of_nodes,int side_node);
#endif
