#include <hdf5.h>
#include "../crosslink/readIn_lib.h"
#include "io.h"
#include <math.h>
#include <stdlib.h>
#include "rng.h"
#include "mergeMolecule_lib.h"



int merge(const char*const filename,bead_data*b,polymer_data*poly){

  /*polymer_data polym;
  polymer_data*const poly=&polym;
  bead_data data;
 
  bead_data*const b=&data;
  data.number_of_beads = NULL;
  data.beads = NULL;
  polym.poly_arch=NULL;
  polym.poly_type=NULL;
  polym.poly_type_offset=NULL;
  polym.boxsize=NULL;

  read_in(filename,b,poly);*/
  
  herr_t status;
  hid_t dataset_id, dataspace_id;  
  hid_t plist_id=H5Pcreate(H5P_FILE_ACCESS); 
  
  hid_t file_id=H5Fopen(filename,H5F_ACC_RDWR,plist_id);
  HDF5_ERROR_CHECK2(file_id,"open file");
  H5Pclose(plist_id);
  plist_id=H5Pcreate(H5P_DATASET_XFER);
  
  unsigned int n_crosslink=b->N_polymers-1;
  unsigned int tmp_poly_arch_length=1;
  unsigned int tmp_number_of_beads=0;
  /*change poly_arch*/
  int * poly_type_checked = (int*) malloc( poly->n_poly_type * sizeof(int));
  int * poly_type_bl_length = (int*) malloc( poly->n_poly_type * sizeof(int));
  
  memset(poly_type_checked,-1,poly->n_poly_type*sizeof(int));
  for(unsigned int poly_i=0;poly_i<b->N_polymers;poly_i++){
    int poly_type_i=poly->poly_type[poly_i];
    int poly_type_offset_i=poly->poly_type_offset[poly_type_i];
    if(poly_type_checked[poly_type_i]==1){
      tmp_poly_arch_length=tmp_poly_arch_length+poly->poly_arch[poly_type_offset_i];
    }
    else{
      int poly_arch_length_i=0;
      if(poly_type_i==poly->n_poly_type-1)
	poly_arch_length_i=poly->poly_arch_length-poly_type_offset_i;
      else
	poly_arch_length_i=poly->poly_type_offset[poly_type_i+1]-poly_type_offset_i;
      tmp_poly_arch_length=tmp_poly_arch_length+poly_arch_length_i-1;
      poly_type_bl_length[poly_type_i]=poly_arch_length_i-1-poly->poly_arch[poly_type_offset_i];
    }
    poly_type_checked[poly_type_i]=1;
    tmp_number_of_beads=tmp_number_of_beads+poly->poly_arch[poly_type_offset_i];
  }
  free(poly_type_checked);
  
  int * poly_arch_map = (int*) malloc(poly->poly_arch_length * sizeof(int));
  memset(poly_arch_map,-1,poly->poly_arch_length*sizeof(int));
  
  uint32_t * tmp_poly_arch = (uint32_t*) malloc( tmp_poly_arch_length * sizeof(uint32_t));
  if(tmp_poly_arch == NULL){
    fprintf(stderr,"Malloc problem %s:%d\n",__FILE__,__LINE__);
    return -4;
  }
  int bl_sequence=0;
  tmp_poly_arch[0]=tmp_number_of_beads;
  for(unsigned int type_i=0;type_i<poly->n_poly_type;type_i++){
    int bl_offset_i=poly->poly_type_offset[type_i]+poly->poly_arch[poly->poly_type_offset[type_i]]+1;
    for(int bl_i=0;bl_i<poly_type_bl_length[type_i];bl_i++){
      tmp_poly_arch[tmp_number_of_beads+1+bl_sequence]=poly->poly_arch[bl_offset_i+bl_i];
      poly_arch_map[bl_offset_i+bl_i]=tmp_number_of_beads+1+bl_sequence;
      bl_sequence++;
    }
  }
  free(poly_type_bl_length);
  
  unsigned int sequence=1;
  for(unsigned int poly_i=0;poly_i<b->N_polymers;poly_i++){
    int poly_type_i=poly->poly_type[poly_i];
    int poly_type_offset_i=poly->poly_type_offset[poly_type_i];
    for(unsigned int mono_i=0;mono_i<b->number_of_beads[poly_i];mono_i++){
      int old_bl=get_bondlist_offset(poly->poly_arch[poly_type_offset_i+1+mono_i]);
      unsigned int mono_i_type=get_particle_type(poly->poly_arch[poly_type_offset_i+1+mono_i]);
      int new_bl=poly_arch_map[old_bl];
      tmp_poly_arch[sequence]=get_info_bl(new_bl,mono_i_type);
      sequence++;
    }
  }  
  free(poly_arch_map);
  hsize_t     dims_poly_arch[1];
  dims_poly_arch[0]=tmp_poly_arch_length;
  dataspace_id = H5Screate_simple(1, dims_poly_arch, NULL);    
  status=H5Ldelete(file_id,"/parameter/poly_arch",H5P_DEFAULT);

  dataset_id = H5Dcreate2(file_id, "/parameter/poly_arch", H5T_NATIVE_INT32,dataspace_id, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);       
  status=H5Dwrite(dataset_id,H5T_NATIVE_INT32,H5S_ALL,H5S_ALL,plist_id,tmp_poly_arch);
  status=H5Dclose(dataset_id);

  /*change poly_type_offset*/
  hsize_t     dims_poly_type_offset[1];
  dims_poly_type_offset[0] = 1;
   
  int *tmp_offset = (int *) malloc(1 * sizeof(int));
  tmp_offset[0]=0;

  dataspace_id = H5Screate_simple(1, dims_poly_type_offset, NULL);
  status=H5Ldelete(file_id,"/parameter/poly_type_offset",H5P_DEFAULT);
  dataset_id = H5Dcreate2(file_id, "/parameter/poly_type_offset", H5T_NATIVE_INT,dataspace_id, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);

  status=H5Dwrite(dataset_id,H5T_NATIVE_INT,H5S_ALL,H5S_ALL,plist_id,tmp_offset);
  status = H5Dclose(dataset_id);

  /*change n_polymers*/
  hsize_t     dims_n_polymers[1];  
  unsigned int tmp_n_polymers=1;
  
  dims_n_polymers[0] = 1;
  
  dataspace_id = H5Screate_simple(1, dims_n_polymers, NULL); 
  status=H5Ldelete(file_id,"/parameter/n_polymers",H5P_DEFAULT);  
  dataset_id = H5Dcreate2(file_id, "/parameter/n_polymers", H5T_NATIVE_UINT,dataspace_id, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);  
  status=H5Dwrite(dataset_id,H5T_NATIVE_UINT,H5S_ALL,H5S_ALL,plist_id,&tmp_n_polymers);
  status=H5Dclose(dataset_id);

  /*change n_poly_type */
 
  hsize_t     dims_n_poly_type[1];
  unsigned int tmp_n_poly_type=1;
  
  dims_n_poly_type[0] = 1;
  
  dataspace_id = H5Screate_simple(1, dims_n_poly_type, NULL);  
  status=H5Ldelete(file_id,"/parameter/n_poly_type",H5P_DEFAULT);  
  dataset_id = H5Dcreate2(file_id, "/parameter/n_poly_type", H5T_NATIVE_UINT,dataspace_id, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);  
  status=H5Dwrite(dataset_id,H5T_NATIVE_UINT,H5S_ALL,H5S_ALL,plist_id,&tmp_n_poly_type);  
  status = H5Dclose(dataset_id);

  /*change array poly_type*/
  hsize_t     dims_poly_type[1];
  dims_poly_type[0] =  tmp_n_polymers;
  unsigned int *tmp_poly_type = (unsigned int * const)malloc(  tmp_n_polymers * sizeof(unsigned int) ); 
  tmp_poly_type[0]=0;  
 
  dataspace_id = H5Screate_simple(1, dims_poly_type, NULL);
  status=H5Ldelete(file_id,"/poly_type",H5P_DEFAULT);
  dataset_id = H5Dcreate2(file_id, "/poly_type", H5T_NATIVE_INT, dataspace_id, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
  status=H5Dwrite(dataset_id,H5T_NATIVE_UINT,H5S_ALL,H5S_ALL,plist_id,tmp_poly_type);
  status = H5Dclose(dataset_id);

  /*change number_of_beads*/

  if( b-> max_n_beads< tmp_number_of_beads)
    b->max_n_beads = tmp_number_of_beads;
 
  /*change size of the beads box*/
  hsize_t     dims_beads[2];
  dims_beads[0] = tmp_n_polymers;  
  dims_beads[1] = b->max_n_beads;
  
  Monomer *tmp_beads = (Monomer * const) malloc(b->max_n_beads*tmp_n_polymers * sizeof(Monomer));
  const hid_t memtype = get_monomer_memtype();
  dataspace_id = H5Screate_simple(2, dims_beads, NULL);
  memcpy(tmp_beads,b->beads, tmp_number_of_beads * sizeof(Monomer));

  status=H5Ldelete(file_id,"/beads",H5P_DEFAULT);
  dataset_id = H5Dcreate2(file_id, "/beads", memtype,dataspace_id, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
  status=H5Dwrite(dataset_id,memtype,H5S_ALL,H5S_ALL,plist_id,tmp_beads);
  status = H5Dclose(dataset_id);


  /*change poly_arch_len*/
  dataset_id=H5Dopen(file_id, "/parameter/poly_arch_length", H5P_DEFAULT);
  status=H5Dwrite(dataset_id,H5T_NATIVE_UINT,H5S_ALL,H5S_ALL,plist_id,&tmp_poly_arch_length);
  status = H5Dclose(dataset_id);  

  /* poly->n_poly_type=tmp_n_poly_type;
  poly->poly_arch=tmp_poly_arch;
  poly->poly_type=tmp_poly_type;
  poly->poly_type_offset=tmp_offset;
  poly->poly_arch_length=tmp_poly_arch_length;
  b->N_polymers=tmp_n_polymers;	*/
  status = H5Fclose(file_id);
  free(tmp_poly_arch);


  return 0;    
}
