#include <stdio.h>
#include <stdlib.h>
#include <hdf5.h>
#include "../crosslink/readIn_lib.h"
#include "rng.h"


int main(int argc, char *argv[])
{
  char *filename = argv[1];
   polymer_data polym;
  polymer_data*const poly=&polym;
  bead_data data;
 
  bead_data*const b=&data;
  data.number_of_beads = NULL;
  data.beads = NULL;
  polym.poly_arch=NULL;
  polym.poly_type=NULL;
  polym.poly_type_offset=NULL;
  polym.boxsize=NULL;

  read_in(filename,b,poly);
  //cross_link_new(filename,b,poly,0,b->N_polymers-1);
  merge(filename,b,poly);
  return 0;
}
