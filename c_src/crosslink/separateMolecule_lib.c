#include <hdf5.h>
#include "readIn_lib.h"
#include "io.h"
#include <math.h>
#include <stdlib.h>
#include "separateMolecule_lib.h"
#include <string.h>
#include "bond.h"
void cross_link_monomer1(int32_t** poly_arch_tmp_pointer,polymer_data*const poly, unsigned int poly_arch_len,unsigned int monomerN, unsigned int monomerM)
{
  unsigned int poly_arch_lenold=poly_arch_len;
  /*load the poly_arch information of monomerN*/
  int32_t* poly_arch_tmp= *poly_arch_tmp_pointer;
  int offset_poly=0;
  int32_t polyarch=poly_arch_tmp[offset_poly+monomerN];

  if(polyarch>4294967200){
    fprintf(stderr, "ERROR: Function get_bondlist_offset exited the range %s:%d\n", __FILE__, __LINE__);
    //return -2;
  }
  int off_bond=get_bondlist_offset(polyarch);
  unsigned int typeN=get_particle_type(poly_arch_tmp[offset_poly+monomerN]);
  int bond_type = HARMONIC;
  do{
    unsigned int end=get_end(poly_arch_tmp[off_bond]);    
    poly_arch_len++;  
    if(end==1) break;  
    off_bond++;    
  }while(0==0);
  /*load the poly_arch information of monomerM*/
  int offset_polyM=0; 
  int bond_exist[poly_arch_len-poly_arch_lenold];//check if the bond already exsists;
  if(polyarch>4294967200){
    fprintf(stderr, "ERROR: Function get_bondlist_offset exited the range %s:%d\n", __FILE__, __LINE__);
    //return -2;
  }
  
  off_bond=get_bondlist_offset(polyarch);
  int32_t *tmp_poly_archM;
  tmp_poly_archM= malloc((poly_arch_len+1) * sizeof(int32_t));
  if(tmp_poly_archM == NULL){
    fprintf(stderr,"Malloc problem %s:%d\n",__FILE__,__LINE__);
  }
 
  memcpy(tmp_poly_archM,poly_arch_tmp,poly_arch_lenold*sizeof(int32_t));
  for(unsigned int i=poly_arch_lenold;i<poly_arch_len;i++){
    int info;
    int offset=get_offset(poly_arch_tmp[off_bond]);
   
    bond_exist[i-poly_arch_lenold]=offset;
    info=get_info(offset, bond_type, 0);
    tmp_poly_archM[i] = info;
    off_bond++;
  }

  int bond_match=0;
  int offset_to_monomerM=offset_polyM+monomerM-offset_poly-monomerN;

  for(unsigned int i=0;i<poly_arch_len-poly_arch_lenold;i++){
    if(offset_to_monomerM==bond_exist[i])
      bond_match++;
  }

  if(bond_match==0 && offset_to_monomerM!=0){
    int info;
    info=get_info(offset_to_monomerM, bond_type,1);
    tmp_poly_archM[poly_arch_len]=info;
    tmp_poly_archM[offset_poly+monomerN] = get_info_bl(poly_arch_lenold, typeN);
     
    poly_arch_len++;   
    *poly_arch_tmp_pointer = tmp_poly_archM;
    poly->poly_arch_length=poly_arch_len;
  }
  
  free(poly_arch_tmp);
}

int separate_molecule(const char* const filename,bead_data*b,polymer_data*poly){

  unsigned int sequence=0;
  int molecule_index=-1;
  int n_poly=0;
  int bond_max=0;
  for(unsigned int i=0;i<b->N_polymers;i++){
    sequence=sequence+b->number_of_beads[i];
  }

  for(int mono_index=poly->poly_type_offset[poly->poly_type[n_poly]]+1;mono_index<sequence+poly->poly_type_offset[poly->poly_type[n_poly]]+1;mono_index++){
    int bonds_of_monomer=0, bond_number=0;
    int32_t  current_poly_arch=poly->poly_arch[mono_index];
    int start_offset_bond=get_bondlist_offset(current_poly_arch);
    do{
      bonds_of_monomer=poly->poly_arch[start_offset_bond];
      int end=get_end(bonds_of_monomer);		
      bond_number++;
      if(end==1) break;
      start_offset_bond++;
    }while(0==0); 
    if(bond_number>bond_max)
      bond_max=bond_number;
  }
  bond_max++;
  int *checked_monomer=(int*) malloc(sequence*sizeof(int));
  if(checked_monomer == NULL){
    fprintf(stderr,"Malloc problem %s:%d\n",__FILE__,__LINE__);
    return -4;
  } 
  memset(checked_monomer,-1,sequence*sizeof(int));

  int *loop_member_length;
  loop_member_length= (int*) malloc(sequence*sizeof(int));
  if(loop_member_length == NULL){
    fprintf(stderr,"Malloc problem %s:%d\n",__FILE__,__LINE__);
    return -4;
  } 
  memset(loop_member_length,0,sequence*sizeof(int));

  int** store_all_crosslink=(int**) malloc(sequence*sizeof(int*));
  if(store_all_crosslink== NULL){
    fprintf(stderr,"Malloc problem %s:%d\n",__FILE__,__LINE__);
    return -4;
  }
  for(unsigned int mono_index=0;mono_index<sequence;mono_index++){
    store_all_crosslink[mono_index]=(int *)malloc(bond_max*sizeof(int));  
    if(store_all_crosslink[mono_index]== NULL){
      fprintf(stderr,"Malloc problem %s:%d\n",__FILE__,__LINE__);
      return -4;
    }
    memset(&store_all_crosslink[mono_index][0],0,bond_max*sizeof(int));
  }  

  /////book place
  int **checked_bonds_node=(int **)malloc(sequence*sizeof(int*));
  if(checked_bonds_node== NULL){
    fprintf(stderr,"Malloc problem %s:%d\n",__FILE__,__LINE__);
    return -4;
  }
  for(unsigned int mono_index=0;mono_index<sequence;mono_index++){
    checked_bonds_node[mono_index]=(int *)malloc(bond_max*sizeof(int));  
    if(checked_bonds_node[mono_index]== NULL){
      fprintf(stderr,"Malloc problem %s:%d\n",__FILE__,__LINE__);
      return -4;
    }
  }   

  int *loop_member=(int*) malloc(sequence*sizeof(int));
  if(loop_member == NULL){
    fprintf(stderr,"Malloc problem %s:%d\n",__FILE__,__LINE__);
    return -4;
  }
  int* nodes_list=(int*) malloc(sequence*sizeof(int));
  if(nodes_list == NULL){
    fprintf(stderr,"Malloc problem %s:%d\n",__FILE__,__LINE__);
    return -4;
  }
  int* nodes_offset=(int*) malloc(sequence*sizeof(int));
  if(nodes_offset == NULL){
    fprintf(stderr,"Malloc problem %s:%d\n",__FILE__,__LINE__);
    return -4;
  }
  int* bond_nodes_checked=(int*) malloc(sequence*sizeof(int));
  if( bond_nodes_checked== NULL){
    fprintf(stderr,"Malloc problem %s:%d\n",__FILE__,__LINE__);
    return -4;
  }
  int* bond_number_total=(int*) malloc(sequence*sizeof(int));
  if( bond_number_total== NULL){
    fprintf(stderr,"Malloc problem %s:%d\n",__FILE__,__LINE__);
    return -4;
    }


  ///book place
  for(unsigned int mono_i=0;mono_i<sequence;mono_i++){
    if(checked_monomer[mono_i]!=-1)
      continue;
    molecule_index++; 
    loop_member_length[molecule_index]=find_molecule(molecule_index,&store_all_crosslink,bond_max,sequence,poly,&checked_monomer,mono_i,loop_member,nodes_list,nodes_offset,bond_nodes_checked,bond_number_total,checked_bonds_node);   
  }

  //free place
  printf("Finished chains booking.\n");
  int start=0;
  int32_t **tmp_poly_arch=(int32_t **)malloc((molecule_index+1)*sizeof(int32_t*));
  if(tmp_poly_arch== NULL){
    fprintf(stderr,"Malloc problem %s:%d\n",__FILE__,__LINE__);
    return -4;
  }
 
  int *tmp_poly_offset=(int *)malloc((molecule_index+1)*sizeof(int));
  if(tmp_poly_offset== NULL){
    fprintf(stderr,"Malloc problem %s:%d\n",__FILE__,__LINE__);
    return -4;
  }
  int *tmp_poly_offset_old=(int *)malloc((molecule_index+1)*sizeof(int));
  if(tmp_poly_offset_old== NULL){
    fprintf(stderr,"Malloc problem %s:%d\n",__FILE__,__LINE__);
    return -4;
  }

  Monomer **tmp_beads=(Monomer ** const)malloc((molecule_index+1)*sizeof(Monomer*));
  if(tmp_beads== NULL){
    fprintf(stderr,"Malloc problem %s:%d\n",__FILE__,__LINE__);
    return -4;
  }
  int max_length=0;
  int total_length=0;
  int total_beads=0;
  int number_poly_type=0;
  int * tmp_poly_type=( int *)malloc((molecule_index+1)*sizeof( int));
  if(tmp_poly_type== NULL){
    fprintf(stderr,"Malloc problem %s:%d\n",__FILE__,__LINE__);
    return -4;
  }

  
  for(int tmp_molecule_index=0; tmp_molecule_index<molecule_index+1;tmp_molecule_index++){
    int* mapping_list;
    mapping_list= (int*) malloc(sequence*sizeof(int));
    if(mapping_list== NULL){
      fprintf(stderr,"Malloc problem %s:%d\n",__FILE__,__LINE__);
      return -4;
    }
    memset(mapping_list,0,sequence*sizeof(int));
    tmp_poly_arch[tmp_molecule_index]= (int32_t*) malloc((total_length+loop_member_length[tmp_molecule_index]*4)*sizeof(int32_t));
    if(tmp_poly_arch[tmp_molecule_index] == NULL){
      fprintf(stderr,"Malloc problem %s:%d\n",__FILE__,__LINE__);
      return -4;
    }
   
    if(tmp_molecule_index!=0){
      memcpy(tmp_poly_arch[tmp_molecule_index],tmp_poly_arch[tmp_molecule_index-1],total_length*sizeof(int32_t));
      free(tmp_poly_arch[tmp_molecule_index-1]);
    }
    start=total_length;
    for(int iii=total_length;iii<total_length+loop_member_length[tmp_molecule_index]*4;iii++){
      tmp_poly_arch[tmp_molecule_index][iii]=0;
    }
  
    tmp_beads[tmp_molecule_index]= (Monomer* const) malloc(loop_member_length[tmp_molecule_index]*sizeof(Monomer));
    if(tmp_beads[tmp_molecule_index] == NULL){
      fprintf(stderr,"Malloc problem %s:%d\n",__FILE__,__LINE__);
      return -4;
    }
    tmp_poly_offset[tmp_molecule_index]=total_length;//changed this
    int exist=write_polyarch(start,tmp_molecule_index,checked_monomer,mapping_list,&tmp_beads,&tmp_poly_arch,sequence,b,poly,store_all_crosslink,n_poly,tmp_poly_offset);
    if(exist==-1){
      if(max_length<tmp_poly_arch[tmp_molecule_index][start])
	max_length=tmp_poly_arch[tmp_molecule_index][start];
      total_beads=total_beads+tmp_poly_arch[tmp_molecule_index][start];
      tmp_poly_type[tmp_molecule_index]=number_poly_type;   
      tmp_poly_offset_old[number_poly_type]=total_length;
      number_poly_type++;
    }
    else{
      total_beads=total_beads+tmp_poly_arch[tmp_molecule_index][tmp_poly_offset[exist]];
      tmp_poly_type[tmp_molecule_index]=tmp_poly_type[exist];
    }
    total_length=poly->poly_arch_length;  
    free(mapping_list);
  } 
  free(checked_monomer);
  int *tmp_poly_offset_new=(int *)malloc((number_poly_type)*sizeof(int));
  if(tmp_poly_offset_new== NULL){
    fprintf(stderr,"Malloc problem %s:%d\n",__FILE__,__LINE__);
    return -4;
  }
  memcpy(tmp_poly_offset_new,tmp_poly_offset_old,number_poly_type*sizeof(int));
  Monomer *beads_total= (Monomer* const) malloc(max_length*(molecule_index+1)*sizeof(Monomer));
  if(beads_total == NULL){
    fprintf(stderr,"Malloc problem %s:%d\n",__FILE__,__LINE__);
    return -4;
  }
  for(int molecule_i=0;molecule_i<molecule_index+1;molecule_i++){
    for(int mono_i=0;mono_i<tmp_poly_arch[molecule_index][tmp_poly_offset_new[tmp_poly_type[molecule_i]]];mono_i++){
      beads_total[mono_i+molecule_i*max_length].x=tmp_beads[molecule_i][mono_i].x;
      beads_total[mono_i+molecule_i*max_length].y=tmp_beads[molecule_i][mono_i].y;
      beads_total[mono_i+molecule_i*max_length].z=tmp_beads[molecule_i][mono_i].z;
    }
    }
  free(loop_member_length);
  herr_t status;
  hid_t dataset_id, dataspace_id;
  hid_t plist_id=H5Pcreate(H5P_FILE_ACCESS);   
  hid_t file_id=H5Fopen(filename,H5F_ACC_RDWR,plist_id);
  HDF5_ERROR_CHECK2(file_id,"open file");
  H5Pclose(plist_id);
  plist_id=H5Pcreate(H5P_DATASET_XFER);
  int n_poly_tmp=molecule_index+1;
  dataset_id=H5Dopen(file_id, "/parameter/n_polymers", H5P_DEFAULT);   
  status=H5Dwrite(dataset_id,H5T_NATIVE_UINT,H5S_ALL,H5S_ALL,plist_id,&n_poly_tmp);
  status = H5Dclose(dataset_id);

  
  hsize_t     dims_poly_archnew[1];
  dims_poly_archnew[0]=total_length;  
  dataspace_id = H5Screate_simple(1, dims_poly_archnew, NULL);
  status=H5Ldelete(file_id,"/parameter/poly_arch",H5P_DEFAULT);
  dataset_id = H5Dcreate2(file_id, "/parameter/poly_arch", H5T_NATIVE_INT32,dataspace_id, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);       
  status=H5Dwrite(dataset_id,H5T_NATIVE_INT32,H5S_ALL,H5S_ALL,plist_id,tmp_poly_arch[molecule_index]);
  status = H5Dclose(dataset_id);
  
  /*change poly_arch_len*/
  dataset_id=H5Dopen(file_id, "/parameter/poly_arch_length", H5P_DEFAULT);
  status=H5Dwrite(dataset_id,H5T_NATIVE_UINT,H5S_ALL,H5S_ALL,plist_id,&total_length);
  status = H5Dclose(dataset_id);

  /*change poly_type*/
  hsize_t     dims_poly_type[1];
  dims_poly_type[0]=molecule_index+1; 
  dataspace_id = H5Screate_simple(1, dims_poly_type, NULL);
  status=H5Ldelete(file_id,"/poly_type",H5P_DEFAULT);
  dataset_id = H5Dcreate2(file_id, "/poly_type", H5T_NATIVE_INT, dataspace_id, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
  status=H5Dwrite(dataset_id,H5T_NATIVE_UINT,H5S_ALL,H5S_ALL,plist_id,tmp_poly_type);
  status = H5Dclose(dataset_id);
  
  free(tmp_poly_type);

    /*change n_poly_type */

  dataset_id=H5Dopen(file_id, "/parameter/n_poly_type", H5P_DEFAULT);
  status=H5Dwrite(dataset_id,H5T_NATIVE_UINT,H5S_ALL,H5S_ALL,plist_id,&number_poly_type);  
  status = H5Dclose(dataset_id);

  
  /*change poly_type_offset */
  hsize_t     dims_poly_type_offset[1];
  dims_poly_type_offset[0] = number_poly_type;  
  dataspace_id = H5Screate_simple(1, dims_poly_type_offset, NULL);
  status=H5Ldelete(file_id,"/parameter/poly_type_offset",H5P_DEFAULT);
  dataset_id = H5Dcreate2(file_id, "/parameter/poly_type_offset", H5T_NATIVE_INT,dataspace_id, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
  status=H5Dwrite(dataset_id,H5T_NATIVE_INT,H5S_ALL,H5S_ALL,plist_id,tmp_poly_offset_new);
  status = H5Dclose(dataset_id);

  free(tmp_poly_offset);
  free(tmp_poly_offset_old);
  free(tmp_poly_offset_new);
  /*change beads*/
  hsize_t     dims_beads[2];
  dims_beads[0] =molecule_index+1;  
  dims_beads[1] =max_length;//change it to max
  const hid_t memtype = get_monomer_memtype();    
  dataspace_id = H5Screate_simple(2, dims_beads, NULL);
  status=H5Ldelete(file_id,"/beads",H5P_DEFAULT);
  dataset_id = H5Dcreate2(file_id, "/beads", memtype,dataspace_id, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
  status=H5Dwrite(dataset_id,memtype,H5S_ALL,H5S_ALL,plist_id,beads_total);
  status = H5Dclose(dataset_id);

  status=H5Fclose(file_id);

  //free(beads_total);

  return 0;
}



int write_polyarch(int start,int tmp_molecule_index,int * checked_monomer,int* mapping_list,Monomer***tmp_beads_pointer,int32_t***tmp_poly_arch_pointer,int sequence,bead_data*b,polymer_data*poly,int** store_all_crosslink,int n_poly,int * tmp_poly_offset){
 
  Monomer** tmp_beads=*tmp_beads_pointer;
  int32_t** tmp_poly_arch=*tmp_poly_arch_pointer;
  int tmp_chain_index=start;
  for(unsigned int tmp_monomer_index=0;tmp_monomer_index<sequence;tmp_monomer_index++){
    if(checked_monomer[tmp_monomer_index]==tmp_molecule_index){
      mapping_list[tmp_monomer_index]=tmp_chain_index;     
      tmp_beads[tmp_molecule_index][tmp_chain_index-start].x=b->beads[tmp_monomer_index].x;
      tmp_beads[tmp_molecule_index][tmp_chain_index-start].y=b->beads[tmp_monomer_index].y;
      tmp_beads[tmp_molecule_index][tmp_chain_index-start].z=b->beads[tmp_monomer_index].z;
      tmp_chain_index++;
    }
  }
  tmp_poly_arch[tmp_molecule_index][start]=tmp_chain_index-start;
  //Last monomer
  int end,offset,bond_type,info;
  bond_type = HARMONIC;
  end = 1;
  offset = -1;
  info=get_info(offset, bond_type, end);
  tmp_poly_arch[tmp_molecule_index][tmp_chain_index+1] = info;
 
  //First monomer
  end = 1;
  offset = 1;
  info = get_info(offset, bond_type, end);
  tmp_poly_arch[tmp_molecule_index][tmp_chain_index+2] = info;
  //Middle monomers A
  end = 0;
  offset = -1;
  info = get_info(offset, bond_type, end);
  tmp_poly_arch[tmp_molecule_index][tmp_chain_index+3] = info;
  
  end = 1;
  offset = 1;
  info = get_info(offset, bond_type, end);
  tmp_poly_arch[tmp_molecule_index][tmp_chain_index+4] = info;

  int tmp_iii=1+start;
  for(unsigned int tmp_monomer_index=0;tmp_monomer_index<sequence;tmp_monomer_index++){   
    if(checked_monomer[tmp_monomer_index]==tmp_molecule_index){
      if(store_all_crosslink[tmp_monomer_index][0]==0){
	if(store_all_crosslink[tmp_monomer_index][1]==1){
	  tmp_poly_arch[tmp_molecule_index][tmp_iii]=get_info_bl(tmp_chain_index+2, get_particle_type(poly->poly_arch[tmp_monomer_index+poly->poly_type_offset[n_poly]+1]));
	}
	if(store_all_crosslink[tmp_monomer_index][1]==-1){
	  tmp_poly_arch[tmp_molecule_index][tmp_iii]=get_info_bl(tmp_chain_index+1, get_particle_type(poly->poly_arch[tmp_monomer_index+poly->poly_type_offset[n_poly]+1]));
	}
	if(store_all_crosslink[tmp_monomer_index][1]==-2){
	  tmp_poly_arch[tmp_molecule_index][tmp_iii]=get_info_bl(tmp_chain_index+3, get_particle_type(poly->poly_arch[tmp_monomer_index+poly->poly_type_offset[n_poly]+1]));
	}	 
      }
      else{	  
	int nachbar=0;
	int which_side=0;
	int jjj=0;
	while(store_all_crosslink[tmp_monomer_index][jjj]!=0){	    
	  if(store_all_crosslink[tmp_monomer_index][jjj]==1||store_all_crosslink[tmp_monomer_index][jjj]==-1){
	    which_side=store_all_crosslink[tmp_monomer_index][jjj];
	    nachbar++;
	  }
	  jjj++;
	}	
	if(nachbar==1){
	  if(which_side==-1)
	    tmp_poly_arch[tmp_molecule_index][tmp_iii]=get_info_bl(tmp_chain_index+1, get_particle_type(poly->poly_arch[tmp_monomer_index+poly->poly_type_offset[n_poly]+1]));
	  if(which_side==1)
	    tmp_poly_arch[tmp_molecule_index][tmp_iii]=get_info_bl(tmp_chain_index+2, get_particle_type(poly->poly_arch[tmp_monomer_index+poly->poly_type_offset[n_poly]+1]));
	}
	  
	if(nachbar==2)	   
	  tmp_poly_arch[tmp_molecule_index][tmp_iii]=get_info_bl(tmp_chain_index+3, get_particle_type(poly->poly_arch[tmp_monomer_index+poly->poly_type_offset[n_poly]+1]));	  
      }
      tmp_iii++;
    }
  }
  poly->poly_arch_length=tmp_chain_index+5;
  int tmp_ii=start;  
  for(unsigned int tmp_monomer_index=0;tmp_monomer_index<sequence;tmp_monomer_index++){  
    if(checked_monomer[tmp_monomer_index]==tmp_molecule_index){	
      if(store_all_crosslink[tmp_monomer_index][0]!=0){
	int jj=0;
	while(store_all_crosslink[tmp_monomer_index][jj]!=0){	    
	  if(store_all_crosslink[tmp_monomer_index][jj]!=-1&&store_all_crosslink[tmp_monomer_index][jj]!=1){
	    cross_link_monomer1(&tmp_poly_arch[tmp_molecule_index],poly,poly->poly_arch_length,tmp_ii+1,mapping_list[store_all_crosslink[tmp_monomer_index][jj]+tmp_monomer_index]+1);     
	   
	  }
	  jj++;
	}
      }
      tmp_ii++;
    }
  }
  int found=0;
  int same_molecule=-1;

  
  if(tmp_molecule_index>0){
    for(int molecule_i=1;molecule_i<=tmp_molecule_index;molecule_i++){
      int match=0;
      int bond_index=tmp_chain_index+1;
      int old_bonds_start=tmp_poly_offset[molecule_i-1]+tmp_poly_arch[tmp_molecule_index][tmp_poly_offset[molecule_i-1]]+1;
      int old_bonds_length=tmp_poly_offset[molecule_i]-old_bonds_start;
      if((poly->poly_arch_length-tmp_chain_index-1!=old_bonds_length)||(tmp_poly_arch[tmp_molecule_index][tmp_poly_offset[molecule_i-1]]!=tmp_poly_arch[tmp_molecule_index][start]))
        continue;
     
      while((tmp_poly_arch[tmp_molecule_index][old_bonds_start]==tmp_poly_arch[tmp_molecule_index][bond_index])&&bond_index<poly->poly_arch_length){
	match++;
	bond_index++;
	old_bonds_start++;
      }  
      
      if(match==old_bonds_length){
	found=1;
	same_molecule=molecule_i-1;
	break;
      }       
    }
  } 
  *tmp_beads_pointer=tmp_beads;
  if(found==0){
    *tmp_poly_arch_pointer=tmp_poly_arch;
    return -1;
  }
  else{
    poly->poly_arch_length=start;
    return same_molecule;
  }
}


int find_molecule(int molecule_index,int*** store_all_crosslink_pointer,int bond_max,int sequence,polymer_data*poly,int ** checked_monomer_pointer,int mono_i,int *loop_member,int* nodes_list,int* nodes_offset, int* bond_nodes_checked,int* bond_number_total,int **checked_bonds_node){

  int normal_node=0;
  int** store_all_crosslink=*store_all_crosslink_pointer;
  int *checked_monomer=*checked_monomer_pointer;
  int n_poly=0;
  int loop_member_index=0;
  int number_of_nodes=0;
  int finish=0;

  int bond_total[bond_max];
  int checked_bonds=0;
  
  memset(bond_total,0,bond_max*sizeof(int));
  memset(loop_member,0,sequence*sizeof(int));
  memset(nodes_list,0,sequence*sizeof(int));
  memset(nodes_offset,0,sequence*sizeof(int));
  memset(bond_number_total,0,sequence*sizeof(int));
  memset(bond_nodes_checked,0,sequence*sizeof(int));
  for(unsigned int mono_index=0;mono_index<sequence;mono_index++){     
    memset(&checked_bonds_node[mono_index][0],0,bond_max*sizeof(int));     
  }
  int current_monomer=poly->poly_type_offset[n_poly]+1+mono_i;
  int molecule_length=0;
  int wang=0;
  while(finish==0){
    molecule_length++;
    checked_monomer[current_monomer-poly->poly_type_offset[n_poly]-1]=molecule_index;      
    memset(bond_total,0,bond_max*sizeof(int));
    int side_node=0;
    int exist=0;
    for(int loop_index_tmp=0;loop_index_tmp<loop_member_index&&exist==0;loop_index_tmp++){
      if(loop_member[loop_index_tmp]==current_monomer){
	exist=1;
	side_node=1;	  
      }
    }

    loop_member_index++;
    loop_member[loop_member_index]=current_monomer;   
    int32_t current_poly_arch=poly->poly_arch[current_monomer];
    int start_offset_bond=get_bondlist_offset(current_poly_arch);
    int bonds_of_monomer;	    
    int bond_number=0;
    int if_end_chain=0;
    int offset_tmp=0;
    int end=0;
    do{
      bonds_of_monomer=poly->poly_arch[start_offset_bond];
      end=get_end(bonds_of_monomer);
      offset_tmp=get_offset(bonds_of_monomer);
      bond_total[bond_number]=get_offset(bonds_of_monomer);		
      bond_number++;
      start_offset_bond++;
    }while(end!=1);
    start_offset_bond--;
   
    if(bond_number==1){
      store_all_crosslink[current_monomer-poly->poly_type_offset[n_poly]-1][0]=0;
      store_all_crosslink[current_monomer-poly->poly_type_offset[n_poly]-1][1]=offset_tmp;
      if(checked_bonds!=0){
	int ii=find_the_lastnode(&number_of_nodes,bond_number_total,bond_nodes_checked);
      if(number_of_nodes<0){
	finish=1;
	break;
      }
      loop_member_index=nodes_offset[number_of_nodes]-1;
      current_monomer=nodes_list[number_of_nodes];
      checked_bonds=0;
      //number_of_nodes=number_of_nodes-ii;
      int tmp=fmin(ii+5,(sequence-number_of_nodes-1));
      memset(&bond_nodes_checked[number_of_nodes+1],0,tmp*sizeof(int));
      memset(&bond_number_total[number_of_nodes+1],0,tmp*sizeof(int));
      for(unsigned int node_tmp=number_of_nodes+1;node_tmp<number_of_nodes+1+tmp;node_tmp++){
	memset(&checked_bonds_node[node_tmp][0],0,bond_max*sizeof(int));	 
      } 
      normal_node=1;
      continue;
      }
    } 

    if(bond_number==2){
      if(bond_total[0]+bond_total[1]==0){
	store_all_crosslink[current_monomer-poly->poly_type_offset[n_poly]-1][0]=0;
	store_all_crosslink[current_monomer-poly->poly_type_offset[n_poly]-1][1]=-2;
      }
      else{
	store_all_crosslink[current_monomer-poly->poly_type_offset[n_poly]-1][0]=bond_total[0];
	store_all_crosslink[current_monomer-poly->poly_type_offset[n_poly]-1][1]=bond_total[1];
      }
    }
    if((bond_number==2&&checked_bonds!=0&&side_node!=1)||(bond_number==1&&checked_bonds==0)){
      normal_node=0;
      int bond_j=find_unchecked_bond(bond_number,bond_total,checked_bonds,checked_bonds_node,number_of_nodes,side_node);
      current_monomer=bond_total[bond_j]+current_monomer;
      checked_bonds=-bond_total[bond_j];
      continue;  
    }
    if(bond_number>=2&&checked_bonds==0){
      side_node=1;
      normal_node=1;
    }
   
    if(bond_number>2||side_node==1){ 
      for(int bond_index=0;bond_index<bond_number;bond_index++){	  
	store_all_crosslink[current_monomer-poly->poly_type_offset[n_poly]-1][bond_index]=bond_total[bond_index];
      }
      int meet_node=0;
      int aa=1;
      int found_the_node=0;
      if(normal_node==0)
	aa=0;
      
      for(int i=0;i<number_of_nodes/*-aa*/;i++){
	if(nodes_list[i]==current_monomer){
	  meet_node=i;
	  found_the_node=1;
	  break;
	}
      }
      if(found_the_node==1){
	for(int j=0;j<bond_number;j++){
	  checked_bonds_node[number_of_nodes][j]=checked_bonds_node[meet_node][j];
	}
      }
      int j=0;	  
      while(checked_bonds_node[number_of_nodes][j]!=0){
	j++;
      }  
      
      bond_nodes_checked[number_of_nodes]=j;
      if(normal_node==0){
	checked_bonds_node[number_of_nodes][j]=checked_bonds;
	bond_nodes_checked[number_of_nodes]++;
	j++;
      }
      if(found_the_node==1){
	bond_nodes_checked[meet_node]=bond_nodes_checked[number_of_nodes];
	for(int jjj=0;jjj<bond_number;jjj++){
	  checked_bonds_node[meet_node][jjj]=checked_bonds_node[number_of_nodes][jjj];
	}	
      }
      if(bond_number==bond_nodes_checked[number_of_nodes]){
	if_end_chain=1;    
      }
      else{	  	    
	int bond_i=find_unchecked_bond(bond_number,bond_total,checked_bonds,checked_bonds_node,number_of_nodes,side_node);
	checked_bonds_node[number_of_nodes][j]=bond_total[bond_i];
	nodes_list[number_of_nodes]=current_monomer;
	nodes_offset[number_of_nodes]=loop_member_index;	
	normal_node=0;	  
	bond_nodes_checked[number_of_nodes]++;	
	current_monomer=current_monomer+bond_total[bond_i];

	if(found_the_node==1){
	    bond_nodes_checked[meet_node]=bond_nodes_checked[number_of_nodes];
	    for(int jjj=0;jjj<bond_number;jjj++){
	      checked_bonds_node[meet_node][jjj]=checked_bonds_node[number_of_nodes][jjj];
	    }
	}
	checked_bonds=-bond_total[bond_i];
      }
    }
    number_of_nodes++;
    bond_number_total[number_of_nodes-1]=bond_number;
    if(if_end_chain==1){
      normal_node=1;
      int ii=find_the_lastnode(&number_of_nodes,bond_number_total,bond_nodes_checked);
      if(number_of_nodes<0){
	finish=1;
	break;
      }
      loop_member_index=nodes_offset[number_of_nodes]-1;
      current_monomer=nodes_list[number_of_nodes];
      
      checked_bonds=0;
      int tmp=fmin(ii+5,(sequence-number_of_nodes-1));
      memset(&bond_nodes_checked[number_of_nodes+1],0,tmp*sizeof(int));
      memset(&bond_number_total[number_of_nodes+1],0,tmp*sizeof(int));
      for(unsigned int node_tmp=number_of_nodes+1;node_tmp<number_of_nodes+1+tmp;node_tmp++){
	memset(&checked_bonds_node[node_tmp][0],0,bond_max*sizeof(int));	 
      }
      continue;
    }
  }//end while finish==0
  
  *checked_monomer_pointer=checked_monomer;
  *store_all_crosslink_pointer=store_all_crosslink;
  return molecule_length;
}

int find_the_lastnode(int* number_of_nodes_pointer, int* bond_number_total,int* bond_nodes_checked){
  int nodes_level=1; 
  int new_node;
  int number_of_nodes=*number_of_nodes_pointer;
  while(number_of_nodes-nodes_level>=0){
    if(bond_number_total[number_of_nodes-nodes_level]!=bond_nodes_checked[number_of_nodes-nodes_level])
      break;
    else	    
      nodes_level++;
    
  }
  new_node=number_of_nodes-nodes_level;
  *number_of_nodes_pointer=new_node;
  return nodes_level;	
}

int find_unchecked_bond(int bond_num_tot,int* bond_total,int checked_bonds,int**checked_bonds_node,int number_of_nodes,int side_node){
  int bond_j;
  if(bond_num_tot<=2&&side_node==0){
    for(bond_j=0;bond_j<bond_num_tot;bond_j++){
      int meetj=0;    
      if(bond_total[bond_j]!=checked_bonds){
	break;
      }       
    }
  }
  else{
    for(bond_j=0;bond_j<bond_num_tot;bond_j++){
      int meetj=0;
      for(int bond_ii=0;bond_ii<bond_num_tot;bond_ii++){
	if(bond_total[bond_j]==checked_bonds_node[number_of_nodes][bond_ii]){
	  meetj=1;
	  break;
	}
      }
      if(meetj==0)
	break;	  
    }
  }
  return bond_j;
}
 
int cut_multiblock_into_diblock(const char* const filename,const int multi,polymer_data*const poly){ 
  herr_t status;
  hid_t dataset_id,dataspace_id;  
  hid_t plist_id=H5Pcreate(H5P_FILE_ACCESS);  
  hid_t file_id=H5Fopen(filename,H5F_ACC_RDWR,plist_id);
  HDF5_ERROR_CHECK2(file_id,"open file");
  H5Pclose(plist_id);
  plist_id=H5Pcreate(H5P_DATASET_XFER);
  
  int32_t* poly_arch=malloc(poly->poly_arch_length*sizeof(int32_t));
  if(poly_arch== NULL){
    fprintf(stderr,"Malloc problem %s:%d\n",__FILE__,__LINE__);
    return -4;
  }
  status = read_hdf5(file_id,"/parameter/poly_arch",H5T_NATIVE_INT32,plist_id,poly_arch);
  for(int multi_index=1;multi_index<multi;multi_index++){
    for(unsigned type_index=0;type_index<poly->n_poly_type;type_index++){
      
      poly_arch[/*32*/poly->poly_type_offset[type_index]+32*multi_index]=get_info_bl(/*68*/poly->poly_arch_length-1, get_particle_type(poly_arch[poly->poly_type_offset[type_index]+32*multi_index]));
      poly_arch[/*33*/poly->poly_type_offset[type_index]+32*multi_index+1]=get_info_bl(/*65*/poly->poly_arch_length-4, get_particle_type(poly_arch[poly->poly_type_offset[type_index]+32*multi_index+1]));
    }
  }
  hsize_t    dims_polyarch[1];
 
  dims_polyarch[0]=poly->poly_arch_length;  
  
  dataspace_id = H5Screate_simple(1, dims_polyarch, NULL);
  
  status=H5Ldelete(file_id,"/parameter/poly_arch",H5P_DEFAULT);
  
  dataset_id = H5Dcreate2(file_id, "/parameter/poly_arch", H5T_NATIVE_INT32,dataspace_id, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT); 
  
  status=H5Dwrite(dataset_id,H5T_NATIVE_INT32,H5S_ALL,H5S_ALL,plist_id,poly_arch);
  status = H5Dclose(dataset_id);
  status=H5Fclose(file_id);
  free(poly_arch);
  return 0;
}
